# Manutenção de laboratórios e exames #

Esse projeto tem como intuito gerenciar laboratórios e exames bem como seus vínculos.

### Arquitetura ###

A arquitetura desse projéto é baseada na Arquitetura Hexagonal. Foi se adotado esse modelo para promover um acoplamento fraco entre as regras de negócio e os frameworks utilizados.

A estrutura base está divida entre três camadas: domain, adapters e infrastructure.
* **domain**: é a camada responsável por conter as regras de negócio, nela vive apenas o bom e velho Java.

* **adapters**: nessa camada podemos encontrar classes e interfaces que fornecem inputs para o dominio, como por exemplo classes que lidam com requisições HTTP ou classes que buscam dados de alguma fonte obscura (repositories).

* **infrastructure**: aqui temos classes que sabem como fazer a aplicação funcionar. Ex: configuração de frameworks como o spring.

### Banco de dados ###

Para a primeira versão essa aplicação foi configurada com o banco H2, um banco em memória. As configurações de conexão estão dentro de **src/main/resources/application.yml**.

### Como executar ###

# Bash

Caso queira executar essa aplicação no bash será necessário ter instalado localmente a JDK11 e o Maven.

Compile o projeto com o projeto com o comando:

```bash
mvn clean install
```

Execute o jar compilado:

```bash
java -jar target/labs-exams-maintenance.jar
```

ps: O nome do jar (labs-exams-maintenance.jar) é gerado baseado em na tag finalName no pom.xml, (dentro da tag de build).

# Docker

Dentro da raiz desse projeto é possível encontrar um Dockerfile que instrui o Docker a criar uma imagem da aplicação.

Gerando uma imagem:

```bash
docker build . -t <nome-do-container>:<tag-de-versao>
```

Após a geração da imagem é só criar o container com:

```bash
docker run -d -p <porta-do-host>:8080 <nome-do-container>:<tag-de-versao>
```

# API

São expostos um conjuntos de APIs REST. Os endpoints são documentados no endpoint <base-app-url>/swagger-ui.html.