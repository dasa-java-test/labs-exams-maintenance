FROM maven:3.6.3-jdk-11 as builder

RUN mkdir -p /source

WORKDIR /source

ADD pom.xml /source
RUN mvn verify clean --fail-never

ADD . /source

RUN mvn package

FROM openjdk:11

COPY --from=builder /source/target/${JAR_NAME} /app

ARG JAR_NAME=labs-exams-maintenance.jar
ENV JAR_NAME=${JAR_NAME}

WORKDIR /app

EXPOSE 8080

CMD java -jar ./${JAR_NAME}
