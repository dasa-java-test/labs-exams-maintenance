package br.com.fas.dasa.labsexamsmaintenance.infrastructure.spring.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.exams.ActiveExamsPort;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.exams.AddExamsPort;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.exams.DeactivateExamPort;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.exams.UpdateExamPort;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.laboratories.GetLaboratoriesPort;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.laboratories.AddLaboratoryPort;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.laboratories.AssociateLabToExamPort;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.laboratories.DeactivateLaboratoryPort;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.laboratories.UpdateLaboratoryPort;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.out.exams.ExamsRepository;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.out.laboratories.LaboratoryRepository;
import br.com.fas.dasa.labsexamsmaintenance.domain.services.exams.ActiveExamsService;
import br.com.fas.dasa.labsexamsmaintenance.domain.services.exams.AddExamsService;
import br.com.fas.dasa.labsexamsmaintenance.domain.services.exams.DeactivateExamService;
import br.com.fas.dasa.labsexamsmaintenance.domain.services.exams.UpdateExamService;
import br.com.fas.dasa.labsexamsmaintenance.domain.services.laboratories.GetLaboratoriesService;
import br.com.fas.dasa.labsexamsmaintenance.domain.services.laboratories.AddLaboratoryService;
import br.com.fas.dasa.labsexamsmaintenance.domain.services.laboratories.AssociateLaboratoryToExamService;
import br.com.fas.dasa.labsexamsmaintenance.domain.services.laboratories.DeactivateLaboratoryService;
import br.com.fas.dasa.labsexamsmaintenance.domain.services.laboratories.UpdateLaboratoryService;

@Configuration
public class BeansDeclaration {
	
	@Bean
	public GetLaboratoriesPort getLaboratoriesPort(LaboratoryRepository repo) {
		return new GetLaboratoriesService(repo);
	}
	
	@Bean
	public AddLaboratoryPort addLaboratoryPort(LaboratoryRepository repository) {
		return new AddLaboratoryService(repository);
	}
	
	@Bean
	public DeactivateLaboratoryPort deactivateLaboratoryPort(LaboratoryRepository laboratoryRepository) {
		return new DeactivateLaboratoryService(laboratoryRepository);
	}
	
	@Bean
	public UpdateLaboratoryPort updateLaboratoryPort(LaboratoryRepository laboratoryRepository) {
		return new UpdateLaboratoryService(laboratoryRepository);
	}
	
	@Bean
	public AssociateLabToExamPort associateLabToExamPort(
			LaboratoryRepository laboratoryRepository,
			ExamsRepository examsRepository
	) {
		return new AssociateLaboratoryToExamService(laboratoryRepository, examsRepository);
	}
	
	@Bean
	public ActiveExamsPort activeExamsPort(ExamsRepository examsRepository) { 
		return new ActiveExamsService(examsRepository);
	}
	
	@Bean
	public AddExamsPort addExamsPort(ExamsRepository examsRepository) {
		return new AddExamsService(examsRepository);
	}
	
	@Bean
	public DeactivateExamPort deactivateExamPort(ExamsRepository examsRepository) {
		return new DeactivateExamService(examsRepository);
	}
	
	@Bean
	public UpdateExamPort updateExamPort(ExamsRepository examsRepository) {
		return new UpdateExamService(examsRepository);
	}

}
