package br.com.fas.dasa.labsexamsmaintenance.domain.services.exams;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Exam;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.exams.AddExamsPort;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.out.exams.ExamsRepository;

public class AddExamsService implements AddExamsPort {
	
	protected final ExamsRepository examsRepository;
	
	public AddExamsService(ExamsRepository examsRepository) {
		this.examsRepository = examsRepository;
	}

	@Override
	public Exam addExam(AddExamCommand command) {
		return examsRepository.addOrUpdateExam(
				fromCommand(command)
		);
	}

	@Override
	public Collection<Exam> addExam(List<AddExamCommand> commands) {
		List<Exam> exams = commands.stream()
			.map(this::fromCommand)
			.collect(Collectors.toList());
		
		return examsRepository.bulkAddOrUpdateExam(exams);
	}
	
	protected Exam fromCommand(AddExamCommand command) {
		return new Exam(command.getName(), command.getType());
	}

}
