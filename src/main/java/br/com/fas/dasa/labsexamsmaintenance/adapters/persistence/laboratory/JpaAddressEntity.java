package br.com.fas.dasa.labsexamsmaintenance.adapters.persistence.laboratory;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Address;

@Entity
@Table(name = "address")
public class JpaAddressEntity {

	@EmbeddedId
	protected JpaAddressEntityID id;
	protected String city;
	protected String street;

	public static JpaAddressEntity from(Address address) {
		JpaAddressEntity entity = new JpaAddressEntity();
		entity.setId(new JpaAddressEntityID(address.getNumber(), address.getZip()));
		entity.setCity(address.getCity());
		entity.setStreet(address.getStreet());
		return entity;
	}

	public Address toAddress() {
		return new Address.Builder().withCity(city).withNumber(id.getNumber()).withStreet(street).withZip(id.getZip())
				.build();
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public JpaAddressEntityID getId() {
		return id;
	}

	public void setId(JpaAddressEntityID id) {
		this.id = id;
	}

}
