package br.com.fas.dasa.labsexamsmaintenance.domain.models;

public class Address {

	private String city;
	private String street;
	private String number;
	private String zip;
	
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}
	
	public static class Builder {
		
		private String _city;
		private String _street;
		private String _number;
		private String _zip;
		
		public Builder withStreet(String street) {
			this._street = street;
			return this;
		}
		
		public Builder withCity(String city) {
			this._city = city;
			return this;
		}
		
		public Builder withNumber(String number) {
			this._number = number;
			return this;
		}
		
		public Builder withZip(String zip) {
			this._zip = zip;
			return this;
		}
		
		public Address build () {
			Address address = new Address();
			
			address.setCity(_city);
			address.setNumber(_number);
			address.setStreet(_street);
			address.setZip(_zip);
			
			return address;
		}
	}

}
