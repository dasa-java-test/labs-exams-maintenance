package br.com.fas.dasa.labsexamsmaintenance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LabsExamsMaintenanceApplication {

	public static void main(String[] args) {
		SpringApplication.run(LabsExamsMaintenanceApplication.class, args);
	}

}
