package br.com.fas.dasa.labsexamsmaintenance.domain.services.laboratories;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Address;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.Laboratory;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.laboratories.UpdateLaboratoryPort;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.out.laboratories.LaboratoryRepository;

public class UpdateLaboratoryService implements UpdateLaboratoryPort {
	
	protected final LaboratoryRepository laboratoryRepository;
	
	public UpdateLaboratoryService(LaboratoryRepository laboratoryRepository) {
		this.laboratoryRepository = laboratoryRepository;
	}
	
	@Override
	public Laboratory updateLaboratory(UpdateLaboratoryCommand updateCommand) {		
		return laboratoryRepository
				.addOrUpdateLaboratory(getUpdateLaboratory(updateCommand));
	}

	@Override
	public Collection<Laboratory> updateLaboratory(List<UpdateLaboratoryCommand> commands) {
		List<Laboratory> laboratories = commands.stream()
			.map(this::getUpdateLaboratory)
			.collect(Collectors.toList());
		
		return laboratoryRepository.bulkAddOrUpdate(laboratories);
	}
	
	protected Laboratory getUpdateLaboratory(UpdateLaboratoryCommand updateCommand) {
		Optional<Laboratory> optionalLab = laboratoryRepository
				.findLaboratoryByID(updateCommand.getLaboratoryId());
		
		if (optionalLab.isEmpty())
			throw new IllegalArgumentException("Laboratory with id " + updateCommand.getLaboratoryId() + " not found!");

		Laboratory lab = optionalLab.get();
		
		if (updateCommand.getName().isPresent())
			lab.setName(updateCommand.getName().get());
		
		if (updateCommand.getStatus().isPresent())
			lab.setStatus(updateCommand.getStatus().get());
		
		if (updateCommand.getAddress().isPresent())
			lab.setAddress(populateAddress(lab, updateCommand.getAddress().get()));
		
		return lab;
	}

	// TODO: Replace for a merge class
	protected Address populateAddress(final Laboratory lab, final Address address) {
		Address toReturn = lab.getAddress();
		
		if (Objects.isNull(toReturn)) {
			return address;
		}
		
		if (Objects.nonNull(address.getCity()))
			toReturn.setCity(address.getCity());
		
		if (Objects.nonNull(address.getNumber()))
			toReturn.setNumber(address.getNumber());
		
		if (Objects.nonNull(address.getStreet()))
			toReturn.setStreet(address.getStreet());
		
		if (Objects.nonNull(address.getZip()))
			toReturn.setZip(address.getZip());
		
		return toReturn;
	}

}
