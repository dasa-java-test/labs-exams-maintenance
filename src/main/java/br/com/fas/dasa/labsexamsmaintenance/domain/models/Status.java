package br.com.fas.dasa.labsexamsmaintenance.domain.models;

public enum Status {
	
	ACTIVE, INACTIVE;

}
