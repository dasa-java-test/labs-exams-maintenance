package br.com.fas.dasa.labsexamsmaintenance.domain.models;

import java.util.UUID;

public class Exam {

	private UUID id;
	private String name;
	private ExamType type;
	private Status status;
	
	public Exam (UUID id, String name, ExamType type, Status status) {
		this.id = id;
		this.name = name;
		this.type = type;
		this.status = status;
	}
	
	public Exam (String name, ExamType type) {
		this(UUID.randomUUID(), name, type, Status.ACTIVE);
	}
	
	public boolean isActive() {
		return this.status == Status.ACTIVE;
	}
	
	public void activate() {
		this.status = Status.ACTIVE;
	}
	
	public void deactivate() {
		this.status = Status.INACTIVE;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ExamType getType() {
		return type;
	}

	public void setType(ExamType type) {
		this.type = type;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Exam other = (Exam) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
