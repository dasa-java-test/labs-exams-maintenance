package br.com.fas.dasa.labsexamsmaintenance.adapters.web.rest.controllers;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.fas.dasa.labsexamsmaintenance.adapters.web.rest.dtos.ErrorResponse;

@ControllerAdvice
public class RestControllerAdvice extends ResponseEntityExceptionHandler {
	
	Logger log = LoggerFactory.getLogger(this.getClass());

	@ExceptionHandler(value = { IllegalArgumentException.class })
	protected ResponseEntity<?> handleBadRequest(IllegalArgumentException ex) {
		log.error("Exception advice IllegalArgumentExceptionerror!", ex);
		return handleResponse(new ErrorResponse(ex.getMessage(), HttpStatus.BAD_REQUEST));
	}

	@ExceptionHandler(value = { Exception.class })
	protected ResponseEntity<?> internalServerError(Exception ex) {
		log.error("Exception advice generic error!", ex);
		return handleResponse(new ErrorResponse(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR));
	}
	
	protected ResponseEntity<?> handleResponse(ErrorResponse response) {
		return new ResponseEntity<>(response, response.status);
	}

}
