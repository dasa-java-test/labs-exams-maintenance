package br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.exams;

import java.util.Collection;
import java.util.List;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Exam;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.ExamType;

public interface AddExamsPort {

	Exam addExam(AddExamCommand command);
	
	Collection<Exam> addExam(List<AddExamCommand> commands);

	final class AddExamCommand {

		private final String name;
		private final ExamType type;

		public AddExamCommand(String name, ExamType type) {
			this.name = name;
			this.type = type;
		}

		public String getName() {
			return name;
		}

		public ExamType getType() {
			return type;
		}

	}

}
