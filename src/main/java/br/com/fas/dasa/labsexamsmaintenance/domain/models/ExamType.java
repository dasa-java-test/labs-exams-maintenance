package br.com.fas.dasa.labsexamsmaintenance.domain.models;

public enum ExamType {
	
	CLINICAL_ANALYSIS, IMAGE; 

}
