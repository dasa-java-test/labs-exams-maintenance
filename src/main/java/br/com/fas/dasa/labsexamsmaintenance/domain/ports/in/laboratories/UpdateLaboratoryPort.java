package br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.laboratories;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Address;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.Laboratory;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.Status;

public interface UpdateLaboratoryPort {

	Laboratory updateLaboratory(UpdateLaboratoryCommand updateCommand);
	
	Collection<Laboratory> updateLaboratory(List<UpdateLaboratoryCommand> commands);

	final class UpdateLaboratoryCommand {

		private final UUID laboratoryId;
		private final Optional<String> name;
		private final Optional<Address> address;
		private final Optional<Status> status;

		public UpdateLaboratoryCommand(UUID laboratoryId, String name, Address address, Status status) {
			this.laboratoryId = laboratoryId;
			this.name = Optional.ofNullable(name);
			this.address = Optional.ofNullable(address);
			this.status = Optional.ofNullable(status);
		}

		public UUID getLaboratoryId() {
			return laboratoryId;
		}

		public Optional<String> getName() {
			return name;
		}

		public Optional<Address> getAddress() {
			return address;
		}

		public Optional<Status> getStatus() {
			return status;
		}

	}

}
