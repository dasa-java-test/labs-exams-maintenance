package br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.laboratories;

import java.util.Collection;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Laboratory;

public interface GetLaboratoriesPort {

	Collection<Laboratory> getActiveLaboratories();
	
	Collection<Laboratory> getLaboratoriesByExamName(String examName);

}
