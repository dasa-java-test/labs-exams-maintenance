package br.com.fas.dasa.labsexamsmaintenance.adapters.persistence.laboratory;

import java.util.stream.Stream;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JpaLaboratoryRepository extends JpaRepository<JpaLaboratoryEntity, String> {
	
	Stream<JpaLaboratoryEntity> findByStatus(String status);
	
	JpaLaboratoryEntity findByIdAndStatus(String id, String status);
	
	Stream<JpaLaboratoryEntity> findByExamsName(String name);
	
}
