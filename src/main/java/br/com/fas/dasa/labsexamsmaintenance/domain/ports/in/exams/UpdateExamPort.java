package br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.exams;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Exam;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.ExamType;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.Status;

public interface UpdateExamPort {

	Exam updateExam(UpdateExamCommand updateCommand);
	
	Collection<Exam> updateExam(List<UpdateExamCommand> commands);

	final class UpdateExamCommand {

		private final UUID id;
		private final Optional<String> name;
		private final Optional<ExamType> type;
		private final Optional<Status> status;

		public UpdateExamCommand(UUID id, String name, ExamType type, Status status) {
			this.id = id;
			this.name = Optional.ofNullable(name);
			this.type = Optional.ofNullable(type);
			this.status = Optional.ofNullable(status);
		}

		public UUID getId() {
			return id;
		}

		public Optional<String> getName() {
			return name;
		}

		public Optional<ExamType> getType() {
			return type;
		}

		public Optional<Status> getStatus() {
			return status;
		}

	}

}
