package br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.exams;

import java.util.Collection;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Exam;

public interface ActiveExamsPort {

	Collection<Exam> getActiveExams();

}
