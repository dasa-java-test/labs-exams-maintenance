package br.com.fas.dasa.labsexamsmaintenance.domain.services.laboratories;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Laboratory;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.laboratories.DeactivateLaboratoryPort;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.out.laboratories.LaboratoryRepository;

public class DeactivateLaboratoryService implements DeactivateLaboratoryPort {
	
	protected final LaboratoryRepository laboratoryRepository;
	
	public DeactivateLaboratoryService(LaboratoryRepository laboratoryRepository) {
		this.laboratoryRepository = laboratoryRepository;
	}

	@Override
	public void deactivateLaboratory(UUID laboratoryId) {
		Laboratory laboratory = getLab(laboratoryId);
		laboratory.deactivate();
		laboratoryRepository.addOrUpdateLaboratory(laboratory);
	}

	@Override
	public void deactivateLaboratory(List<UUID> labsIds) {
		List<Laboratory> laboratories = labsIds.stream()
			.map(this::getLab)
			.map(lab -> {
				lab.deactivate();
				return lab;
			})
			.collect(Collectors.toList());
		laboratoryRepository.bulkAddOrUpdate(laboratories);		
	}
	
	protected Laboratory getLab(UUID laboratoryId) {
		Optional<Laboratory> optionalLaboratory = laboratoryRepository
				.findLaboratoryByID(laboratoryId);
		
		if (optionalLaboratory.isEmpty())
			throw new IllegalArgumentException("Laboratory with id " + laboratoryId + " not found!");

		return optionalLaboratory.get();
	}

}
