package br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.laboratories;

import java.util.Collection;
import java.util.List;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Address;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.Laboratory;

public interface AddLaboratoryPort {

	Laboratory addLaboratory(AddLaboratoryCommand command);
	
	Collection<Laboratory> addLaboratory(List<AddLaboratoryCommand> commands);

	final class AddLaboratoryCommand {

		private final String name;
		private final Address address;

		public AddLaboratoryCommand(String name, Address address) {
			this.name = name;
			this.address = address;
		}

		public String getName() {
			return name;
		}

		public Address getAddress() {
			return address;
		}

	}

}
