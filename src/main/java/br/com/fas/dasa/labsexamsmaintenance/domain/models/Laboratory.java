package br.com.fas.dasa.labsexamsmaintenance.domain.models;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

public class Laboratory {

	private UUID id;
	private String name;
	private Address address;
	private Status status;
	private Set<Exam> exams;

	public Laboratory(UUID id, String name, Address address, Status status, Set<Exam> exams) {
		this.id = id;
		this.name = name;
		this.address = address;
		this.exams = exams;
		this.status = status;
	}

	public static Laboratory of(String name, Address address) {
		return of(name, address, new HashSet<Exam>());
	}

	public static Laboratory of(String name, Address address, Set<Exam> exams) {
		return new Laboratory(UUID.randomUUID(), name, address, Status.ACTIVE, exams);
	}

	public boolean isActive() {
		return this.status == Status.ACTIVE;
	}

	public void activate() {
		this.status = Status.ACTIVE;
	}

	public void deactivate() {
		this.status = Status.INACTIVE;
	}

	public void associateExam(Exam exam) {
		if (Objects.isNull(exam))
			throw new IllegalArgumentException("Cannot associate null exam!");
		if (exam.getStatus() == Status.INACTIVE)
			throw new IllegalArgumentException("Cannot associate inactive exam!");
		this.exams.add(exam);
	}

	public void disassociateExam(Exam exam) {
		if (Objects.isNull(exam))
			throw new IllegalArgumentException("Cannot disassociate null exam!");
		this.exams.remove(exam);
	}

	public boolean containsExam(UUID examID) {
		return exams.stream().filter(exam -> exam.getId().equals(examID)).findFirst().isPresent();
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Status getStatus() {
		return this.status;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Set<Exam> getExams() {
		return exams;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Laboratory other = (Laboratory) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
