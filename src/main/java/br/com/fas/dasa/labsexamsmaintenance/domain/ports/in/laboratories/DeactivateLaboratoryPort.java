package br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.laboratories;

import java.util.List;
import java.util.UUID;

public interface DeactivateLaboratoryPort {

	void deactivateLaboratory(UUID laboratoryId);
	
	void deactivateLaboratory(List<UUID> labs);

}
