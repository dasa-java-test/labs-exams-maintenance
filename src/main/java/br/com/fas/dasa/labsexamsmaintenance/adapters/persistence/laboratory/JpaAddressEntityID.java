package br.com.fas.dasa.labsexamsmaintenance.adapters.persistence.laboratory;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotEmpty;

@Embeddable
public class JpaAddressEntityID implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4300494167843697362L;

	@NotEmpty(message = "Numero do endereço é requerido!")
	protected String number;

	@NotEmpty(message = "CEP do endereço é requerido!")
	protected String zip;

	public JpaAddressEntityID() {
	}

	public JpaAddressEntityID(String number, String zip) {
		this.number = number;
		this.zip = zip;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result + ((zip == null) ? 0 : zip.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JpaAddressEntityID other = (JpaAddressEntityID) obj;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (zip == null) {
			if (other.zip != null)
				return false;
		} else if (!zip.equals(other.zip))
			return false;
		return true;
	}

}
