package br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.laboratories;

import java.util.UUID;

public interface AssociateLabToExamPort {
	
	void associateExamToLaboratory(UUID labID, UUID examId);

}
