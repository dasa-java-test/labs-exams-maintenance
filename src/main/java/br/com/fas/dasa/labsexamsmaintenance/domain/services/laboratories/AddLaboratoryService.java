package br.com.fas.dasa.labsexamsmaintenance.domain.services.laboratories;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Laboratory;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.laboratories.AddLaboratoryPort;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.out.laboratories.LaboratoryRepository;

public class AddLaboratoryService implements AddLaboratoryPort {
	
	protected final LaboratoryRepository addLaboratoryRepository;
	
	public AddLaboratoryService(LaboratoryRepository repository) {
		this.addLaboratoryRepository = repository;
	}

	@Override
	public Laboratory addLaboratory(AddLaboratoryCommand command) {
		return addLaboratoryRepository.addOrUpdateLaboratory(
				fromCommand(command));
	}

	@Override
	public Collection<Laboratory> addLaboratory(List<AddLaboratoryCommand> commands) {
		List<Laboratory> labs = commands.stream()
			.map(this::fromCommand)
			.collect(Collectors.toList());
		return addLaboratoryRepository.bulkAddOrUpdate(labs);
	}
	
	private Laboratory fromCommand(AddLaboratoryCommand command) {
		return Laboratory.of(command.getName(), command.getAddress());
	}

}
