package br.com.fas.dasa.labsexamsmaintenance.domain.services.exams;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Exam;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.exams.DeactivateExamPort;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.out.exams.ExamsRepository;

public class DeactivateExamService implements DeactivateExamPort {
	
	protected final ExamsRepository examsRepository;
	
	public DeactivateExamService(ExamsRepository examsRepository) {
		this.examsRepository = examsRepository;
	}

	@Override
	public void deactivateExam(UUID id) {
		Exam exam = fromID(id);
		exam.deactivate();
		examsRepository.addOrUpdateExam(exam);
	}

	@Override
	public void deactivateExam(List<UUID> ids) {
		List<Exam> exams = ids.stream()
			.map(this::fromID)
			.map(exam -> {
				exam.deactivate();
				return exam;
			})
			.collect(Collectors.toList());
		
		examsRepository.bulkAddOrUpdateExam(exams);
	}
	
	protected Exam fromID(UUID id) {
		Optional<Exam> optional = examsRepository
				.findExamByID(id);
		
		if (optional.isEmpty())
			throw new IllegalArgumentException("Exam with id " + id + " not found!");
		
		return optional.get();
	}

}
