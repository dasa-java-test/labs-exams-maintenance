package br.com.fas.dasa.labsexamsmaintenance.adapters.web.rest.dtos;

import java.util.Objects;
import java.util.UUID;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.ExamType;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.Status;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.exams.AddExamsPort;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.exams.UpdateExamPort;

public class ExamDTO {

	public String id;
	
	@NotEmpty(message = "Nome é requerido")
	public String name;
	
	@NotEmpty(message = "Tipo é requerido")
	public String type;
	
	public String status;

	public AddExamsPort.AddExamCommand toAddCommand() {
		return new AddExamsPort.AddExamCommand(name, getModelType());
	}
	
	public UpdateExamPort.UpdateExamCommand toUpdateCommand() {		
		return new UpdateExamPort.UpdateExamCommand(
				UUID.fromString(id), name, getModelType(), getModelStatus());
	}
	
	@JsonIgnore
	public ExamType getModelType() {
		if (Objects.isNull(type)) return null;
		
		try {
			return ExamType.valueOf(type.toUpperCase());
		} catch (IllegalArgumentException e) {
			throw new IllegalArgumentException(
					String.format("Unknown exam type %s", type), e
			);
		}
	}
	
	@JsonIgnore
	public Status getModelStatus() {
		if (Objects.isNull(status)) return null;
		
		try {
			return Status.valueOf(status.toUpperCase());
		} catch (IllegalArgumentException e) {
			throw new IllegalArgumentException(
					String.format("Unknown Status %s", status), e
			);
		}
	}

}
