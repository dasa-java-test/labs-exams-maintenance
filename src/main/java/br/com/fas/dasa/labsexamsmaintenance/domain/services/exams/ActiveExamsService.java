package br.com.fas.dasa.labsexamsmaintenance.domain.services.exams;

import java.util.Collection;
import java.util.stream.Collectors;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Exam;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.exams.ActiveExamsPort;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.out.exams.ExamsRepository;

public class ActiveExamsService implements ActiveExamsPort {
	
	protected final ExamsRepository examsRepository;
	
	public ActiveExamsService(ExamsRepository examsRepository) {
		this.examsRepository = examsRepository;
	}

	@Override
	public Collection<Exam> getActiveExams() {
		return this.examsRepository.findAllActiveExams()
				.stream()
				.filter(exam -> exam.isActive())
				.collect(Collectors.toList());
	}

}
