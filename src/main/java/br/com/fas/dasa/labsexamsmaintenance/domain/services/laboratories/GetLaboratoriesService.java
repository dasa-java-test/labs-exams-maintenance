package br.com.fas.dasa.labsexamsmaintenance.domain.services.laboratories;

import java.util.Collection;
import java.util.stream.Collectors;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Laboratory;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.laboratories.GetLaboratoriesPort;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.out.laboratories.LaboratoryRepository;

public class GetLaboratoriesService implements GetLaboratoriesPort {
	
	protected final LaboratoryRepository laboratoryRepository;
	
	public GetLaboratoriesService(LaboratoryRepository laboratoryRepository) {
		this.laboratoryRepository = laboratoryRepository;
	}

	@Override
	public Collection<Laboratory> getActiveLaboratories() {
		return this.laboratoryRepository.findAllActiveLaboratories()
				.stream()
				.filter(lab -> lab.isActive())
				.collect(Collectors.toList());
	}

	@Override
	public Collection<Laboratory> getLaboratoriesByExamName(String examName) {
		return this.laboratoryRepository.findAllByExamName(examName);
	}

}
