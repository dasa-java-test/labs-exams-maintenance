package br.com.fas.dasa.labsexamsmaintenance.domain.services.exams;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Exam;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.exams.UpdateExamPort;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.out.exams.ExamsRepository;

public class UpdateExamService implements UpdateExamPort {
	
	protected final ExamsRepository examsRepository;
	
	public UpdateExamService(ExamsRepository examsRepository) {
		this.examsRepository = examsRepository;
	}
	
	@Override
	public Exam updateExam(UpdateExamCommand updateCommand) {
		return examsRepository.addOrUpdateExam(fromCommand(updateCommand));
	}

	@Override
	public Collection<Exam> updateExam(List<UpdateExamCommand> commands) {
		List<Exam> exams = commands.stream()
			.map(this::fromCommand)
			.collect(Collectors.toList());

		return examsRepository.bulkAddOrUpdateExam(exams);
	}
	
	protected Exam fromCommand(UpdateExamCommand updateCommand) {
		Optional<Exam> optional = examsRepository
				.findExamByID(updateCommand.getId());
		
		if (optional.isEmpty())
			throw new IllegalArgumentException("Exam with id " + updateCommand.getId() + " not found!");

		Exam exam = optional.get();
		
		if (updateCommand.getName().isPresent())
			exam.setName(updateCommand.getName().get());
		
		if (updateCommand.getType().isPresent())
			exam.setType(updateCommand.getType().get());
		
		if (updateCommand.getStatus().isPresent())
			exam.setStatus(updateCommand.getStatus().get());
		
		return exam;
	}

}
