package br.com.fas.dasa.labsexamsmaintenance.adapters.persistence.exam;

import java.util.stream.Stream;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JpaExamRepository extends JpaRepository<JpaExamEntity, String> {
	
	Stream<JpaExamEntity> findByStatus(String status);

}
