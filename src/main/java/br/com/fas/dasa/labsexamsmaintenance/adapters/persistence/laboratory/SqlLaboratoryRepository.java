package br.com.fas.dasa.labsexamsmaintenance.adapters.persistence.laboratory;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Laboratory;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.Status;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.out.laboratories.LaboratoryRepository;

@Component
public class SqlLaboratoryRepository implements LaboratoryRepository {
	
	@Autowired
	protected JpaLaboratoryRepository jpaLaboratoryRepository;

	@Override
	@Transactional
	public List<Laboratory> findAllActiveLaboratories() {
		return jpaLaboratoryRepository.findByStatus(Status.ACTIVE.toString())
				.map(entity -> entity.toLaboratory())
				.collect(Collectors.toList());
	}

	@Override
	public Optional<Laboratory> findLaboratoryByID(UUID laboratoryId) {	
		Optional<JpaLaboratoryEntity> lab = jpaLaboratoryRepository
				.findById(laboratoryId.toString());
		
		return lab.isPresent()
				? Optional.of(lab.get().toLaboratory())
				: Optional.empty();
	}

	@Override
	public Optional<Laboratory> findActiveLaboratoryByID(UUID laboratoryId) {
		JpaLaboratoryEntity laboratory = jpaLaboratoryRepository.findByIdAndStatus(
				laboratoryId.toString(), Status.ACTIVE.toString());
		
		return Objects.nonNull(laboratory)
				? Optional.of(laboratory.toLaboratory())
				: Optional.empty();
	}

	@Override
	public Laboratory addOrUpdateLaboratory(Laboratory laboratory) {
		JpaLaboratoryEntity entity = JpaLaboratoryEntity.from(laboratory);
		JpaLaboratoryEntity saved = jpaLaboratoryRepository.save(entity);
		return saved.toLaboratory();
	}

	@Override
	public List<Laboratory> bulkAddOrUpdate(List<Laboratory> laboratories) {
		List<JpaLaboratoryEntity> labEntities = laboratories.stream()
			.map(JpaLaboratoryEntity::from)
			.collect(Collectors.toList());
		
		List<JpaLaboratoryEntity> saved = jpaLaboratoryRepository.saveAll(labEntities);
		
		return saved.stream()
			.map(entity -> entity.toLaboratory())
			.collect(Collectors.toList());
	}

	@Override
	@Transactional
	public List<Laboratory> findAllByExamName(String examName) {
		return jpaLaboratoryRepository.findByExamsName(examName)
				.map(entity -> entity.toLaboratory())
				.collect(Collectors.toList());
	}

}
