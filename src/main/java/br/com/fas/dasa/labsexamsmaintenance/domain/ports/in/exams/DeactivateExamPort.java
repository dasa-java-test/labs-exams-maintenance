package br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.exams;

import java.util.List;
import java.util.UUID;

public interface DeactivateExamPort {

	void deactivateExam(UUID id);

	void deactivateExam(List<UUID> id);

}
