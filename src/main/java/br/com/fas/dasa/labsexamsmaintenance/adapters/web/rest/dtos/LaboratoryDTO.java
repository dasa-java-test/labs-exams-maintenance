package br.com.fas.dasa.labsexamsmaintenance.adapters.web.rest.dtos;

import java.util.Objects;
import java.util.UUID;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Address;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.Status;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.laboratories.AddLaboratoryPort;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.laboratories.UpdateLaboratoryPort;

public class LaboratoryDTO {
	
	public String id;
	
	@NotEmpty(message = "Nome é requerido")
	public String name;

	public String status;

//	@NotEmpty(message = "Endereço é requerido")
	public Address address;
	
	public AddLaboratoryPort.AddLaboratoryCommand toAddLaboratoryCommand() {
		return new AddLaboratoryPort.AddLaboratoryCommand(this.name, this.address);
	}
	
	public UpdateLaboratoryPort.UpdateLaboratoryCommand toUpdateLaboratoryCommand() {
		return new UpdateLaboratoryPort.UpdateLaboratoryCommand(
				UUID.fromString(id), name, address, getModelStatus());
	}
	
	@JsonIgnore
	public Status getModelStatus() {
		if (Objects.isNull(status)) return null;
		
		try {
			return Status.valueOf(status.toUpperCase());
		} catch (IllegalArgumentException e) {
			throw new IllegalArgumentException(
					String.format("Unknown Status %s", status), e
			);
		}
	}

}
