package br.com.fas.dasa.labsexamsmaintenance.adapters.persistence.exam;

import java.util.Set;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import br.com.fas.dasa.labsexamsmaintenance.adapters.persistence.laboratory.JpaLaboratoryEntity;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.Exam;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.ExamType;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.Status;

@Entity
@Table(name = "exam")
public class JpaExamEntity {

	@Id
	protected String id;

	protected String name;

	protected String type;

	protected String status;
	
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "exams", cascade = CascadeType.ALL)
	protected Set<JpaLaboratoryEntity> laboratories;

	public JpaExamEntity() {
	}

	public JpaExamEntity(String id, String name, String type, String status) {
		this.id = id;
		this.name = name;
		this.type = type;
		this.status = status;
	}

	public static JpaExamEntity from(Exam exam) {
		return new JpaExamEntity(exam.getId().toString(), exam.getName(),
				exam.getType().toString(),
				exam.getStatus().toString());
	}

	public Exam toExam() {
		return new Exam(UUID.fromString(id), name, ExamType.valueOf(type), Status.valueOf(status));
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
