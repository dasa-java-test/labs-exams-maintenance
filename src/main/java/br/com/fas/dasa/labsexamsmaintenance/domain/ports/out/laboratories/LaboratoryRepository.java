package br.com.fas.dasa.labsexamsmaintenance.domain.ports.out.laboratories;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Laboratory;

public interface LaboratoryRepository {
	
	List<Laboratory> findAllActiveLaboratories();
	
	Optional<Laboratory> findLaboratoryByID(UUID laboratoryId);
	
	Optional<Laboratory> findActiveLaboratoryByID(UUID laboratoryId);
	
	Laboratory addOrUpdateLaboratory(Laboratory laboratory);
	
	List<Laboratory> bulkAddOrUpdate(List<Laboratory> laboratories);
	
	List<Laboratory> findAllByExamName(String examName);

}
