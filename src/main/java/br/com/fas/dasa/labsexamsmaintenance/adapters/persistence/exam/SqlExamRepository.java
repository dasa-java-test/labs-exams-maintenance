package br.com.fas.dasa.labsexamsmaintenance.adapters.persistence.exam;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Exam;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.Status;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.out.exams.ExamsRepository;

@Component
public class SqlExamRepository implements ExamsRepository {
	
	@Autowired
	protected JpaExamRepository jpaExamRepository;

	@Override
	@Transactional
	public List<Exam> findAllActiveExams() {
		return jpaExamRepository
			.findByStatus(Status.ACTIVE.toString())
			.map(entity -> entity.toExam())
			.collect(Collectors.toList());
	}

	@Override
	public Optional<Exam> findExamByID(UUID id) {
		Optional<JpaExamEntity> optional = jpaExamRepository
			.findById(id.toString());
		
		return optional.isPresent()
			? Optional.of(optional.get().toExam())
			: Optional.empty();
	}

	@Override
	public Exam addOrUpdateExam(Exam exam) {
		JpaExamEntity entity = JpaExamEntity.from(exam);
		JpaExamEntity saved = jpaExamRepository.save(entity);
		return saved.toExam();
	}

	@Override
	public List<Exam> bulkAddOrUpdateExam(List<Exam> exams) {
		List<JpaExamEntity> entities = exams.stream()
			.map(JpaExamEntity::from)
			.collect(Collectors.toList());
		
		return jpaExamRepository.saveAll(entities)
				.stream()
				.map(entity -> entity.toExam())
				.collect(Collectors.toList());
	}

}
