package br.com.fas.dasa.labsexamsmaintenance.adapters.persistence.laboratory;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.fas.dasa.labsexamsmaintenance.adapters.persistence.exam.JpaExamEntity;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.Address;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.Exam;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.Laboratory;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.Status;

@Entity
@Table(name = "laboratory")
public class JpaLaboratoryEntity {

	@Id
	protected String id;

	protected String name;

	protected String status;

	@OneToOne(cascade = CascadeType.ALL)
	protected JpaAddressEntity address;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "laboratory_exams")
	protected Set<JpaExamEntity> exams = new HashSet<>();

	public static JpaLaboratoryEntity from(Laboratory lab) {
		JpaLaboratoryEntity entity = new JpaLaboratoryEntity();
		entity.setId(lab.getId().toString());
		entity.setName(lab.getName());
		entity.setStatus(lab.getStatus().toString());
		entity.setAddress(lab.getAddress());
		
		if (Objects.nonNull(lab.getExams())) {
			Set<JpaExamEntity> examEntities = lab.getExams().stream().map(JpaExamEntity::from)
				.collect(Collectors.toSet());
			entity.setExams(examEntities);
		}
		
		return entity;
	}

	public Laboratory toLaboratory() {
		Set<Exam> mappedExams = exams.stream().map(exam -> exam.toExam()).collect(Collectors.toSet());

		return new Laboratory(UUID.fromString(id), name, address.toAddress(), Status.valueOf(status), mappedExams);
	}

	public void setAddress(Address address) {
		setAddress(JpaAddressEntity.from(address));
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public JpaAddressEntity getAddress() {
		return address;
	}

	public void setAddress(JpaAddressEntity address) {
		this.address = address;
	}

	public Set<JpaExamEntity> getExams() {
		return exams;
	}

	public void setExams(Set<JpaExamEntity> exams) {
		this.exams = exams;
	}

}
