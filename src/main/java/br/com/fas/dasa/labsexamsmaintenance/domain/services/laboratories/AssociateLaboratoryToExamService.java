package br.com.fas.dasa.labsexamsmaintenance.domain.services.laboratories;

import java.util.Optional;
import java.util.UUID;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Exam;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.Laboratory;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.laboratories.AssociateLabToExamPort;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.out.exams.ExamsRepository;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.out.laboratories.LaboratoryRepository;

public class AssociateLaboratoryToExamService implements AssociateLabToExamPort {
	
	protected LaboratoryRepository laboratoryRepository;
	protected ExamsRepository examsRepository;
	
	public AssociateLaboratoryToExamService(
			LaboratoryRepository laboratoryRepository,
			ExamsRepository examsRepository
	) {
		this.laboratoryRepository = laboratoryRepository;
		this.examsRepository = examsRepository;
	}

	@Override
	public void associateExamToLaboratory(UUID labID, UUID examId) {
		Optional<Laboratory> optionalLab = this.laboratoryRepository
				.findActiveLaboratoryByID(labID);
		
		if (optionalLab.isEmpty())
			throw new IllegalArgumentException("Laboratory " + labID + " not found or inactive!");
		
		Optional<Exam> optionalExam = this.examsRepository.findExamByID(examId);
		
		if (optionalExam.isEmpty())
			throw new IllegalArgumentException("Exam " + labID + " not found!");
		
		Laboratory laboratory = optionalLab.get();
		Exam exam = optionalExam.get();
		
		laboratory.associateExam(exam);
		laboratoryRepository.addOrUpdateLaboratory(laboratory);
	}

}
