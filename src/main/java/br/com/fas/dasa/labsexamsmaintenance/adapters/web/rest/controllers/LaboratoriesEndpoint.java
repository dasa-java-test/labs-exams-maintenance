package br.com.fas.dasa.labsexamsmaintenance.adapters.web.rest.controllers;

import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.fas.dasa.labsexamsmaintenance.adapters.web.rest.dtos.BatchDTO;
import br.com.fas.dasa.labsexamsmaintenance.adapters.web.rest.dtos.LaboratoryDTO;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.Laboratory;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.laboratories.GetLaboratoriesPort;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.laboratories.AddLaboratoryPort;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.laboratories.AddLaboratoryPort.AddLaboratoryCommand;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.laboratories.AssociateLabToExamPort;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.laboratories.DeactivateLaboratoryPort;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.laboratories.UpdateLaboratoryPort;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.laboratories.UpdateLaboratoryPort.UpdateLaboratoryCommand;

@Controller
@RequestMapping(
		path = "laboratories",
		produces = MediaType.APPLICATION_JSON_VALUE
)
public class LaboratoriesEndpoint {
	
	@Autowired
	protected GetLaboratoriesPort activeLabs;
	
	@Autowired
	protected AddLaboratoryPort addLaboratory;
	
	@Autowired
	protected UpdateLaboratoryPort updateLaboratoryPort;
	
	@Autowired
	protected DeactivateLaboratoryPort deactivateLaboratoryPort;
	
	@Autowired
	protected AssociateLabToExamPort associateLabToExamPort;
	
	@GetMapping
	public ResponseEntity<Collection<Laboratory>> getActiveLabs() {
		Collection<Laboratory> activeLaboratories = this.activeLabs.getActiveLaboratories();
		return ResponseEntity.ok(activeLaboratories);
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Laboratory> createLaboratory(@Valid @RequestBody LaboratoryDTO lab) {
		Laboratory created = this.addLaboratory
				.addLaboratory(lab.toAddLaboratoryCommand());
		return new ResponseEntity<Laboratory>(created, HttpStatus.CREATED);
	}
	
	@PatchMapping(path = "{labID}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Laboratory> updateLaboratory(
			@PathVariable("labID") String labID,
			@RequestBody LaboratoryDTO lab
	) {
		lab.id = labID;
		Laboratory updated = this.updateLaboratoryPort
				.updateLaboratory(lab.toUpdateLaboratoryCommand());
		return new ResponseEntity<Laboratory>(updated, HttpStatus.OK);
	}
	
	@DeleteMapping(path = "{labID}")
	public ResponseEntity<?> deactivateLaboratory(@PathVariable("labID") String labID) {
		this.deactivateLaboratoryPort
			.deactivateLaboratory(UUID.fromString(labID));
		return ResponseEntity.ok().build();
	}
	
	@PatchMapping(path = "{labID}/bind/{examID}")
	public ResponseEntity<Laboratory> createLaboratory(
			@PathVariable("labID") UUID labID,
			@PathVariable("examID") UUID examID
	) {
		this.associateLabToExamPort
			.associateExamToLaboratory(labID, examID);
		return ResponseEntity.ok().build();
	}
	
	@PostMapping(path = "batch", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<Laboratory>> batchCreate(@Valid @RequestBody BatchDTO<LaboratoryDTO> dtos) {
		List<AddLaboratoryCommand> commands = dtos.batch.stream()
			.map(lab -> lab.toAddLaboratoryCommand())
			.collect(Collectors.toList());
		
		Collection<Laboratory> created = this.addLaboratory
				.addLaboratory(commands);
		
		return new ResponseEntity<>(created, HttpStatus.CREATED);
	}
	
	@PatchMapping(path = "batch", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<Laboratory>> updateLaboratory(
			@RequestBody BatchDTO<LaboratoryDTO> dtos
	) {
		List<UpdateLaboratoryCommand> commands = dtos.batch.stream()
				.map(lab -> lab.toUpdateLaboratoryCommand())
				.collect(Collectors.toList());
		
		Collection<Laboratory> updated = this.updateLaboratoryPort
				.updateLaboratory(commands);
		return new ResponseEntity<>(updated, HttpStatus.OK);
	}
	
	@DeleteMapping(path = "batch")
	public ResponseEntity<?> deactivateLaboratory(@RequestBody BatchDTO<UUID> ids) {
		this.deactivateLaboratoryPort
			.deactivateLaboratory(ids.batch);
		return ResponseEntity.ok().build();
	}
}
