package br.com.fas.dasa.labsexamsmaintenance.adapters.web.rest.dtos;

import java.util.Arrays;
import java.util.List;

public class BatchDTO<T> {

	public List<T> batch;
	
	public BatchDTO() {}

	@SafeVarargs
	public BatchDTO(T... batch) {
		super();
		this.batch = Arrays.asList(batch);
	}

}
