package br.com.fas.dasa.labsexamsmaintenance.domain.ports.out.exams;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Exam;

public interface ExamsRepository {
	
	List<Exam> findAllActiveExams();
	
	Optional<Exam> findExamByID(UUID id);
	
	Exam addOrUpdateExam(Exam exam);
	
	List<Exam> bulkAddOrUpdateExam(List<Exam> exams);

}
