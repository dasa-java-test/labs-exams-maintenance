package br.com.fas.dasa.labsexamsmaintenance.adapters.web.rest.controllers;

import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.fas.dasa.labsexamsmaintenance.adapters.web.rest.dtos.BatchDTO;
import br.com.fas.dasa.labsexamsmaintenance.adapters.web.rest.dtos.ExamDTO;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.Exam;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.Laboratory;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.exams.ActiveExamsPort;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.exams.AddExamsPort;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.exams.AddExamsPort.AddExamCommand;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.exams.DeactivateExamPort;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.exams.UpdateExamPort;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.exams.UpdateExamPort.UpdateExamCommand;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.laboratories.GetLaboratoriesPort;

@Controller
@RequestMapping(
		path = "exams",
		produces = MediaType.APPLICATION_JSON_VALUE
)
public class ExamsEndpoint {
	
	@Autowired
	protected GetLaboratoriesPort getLaboratoriesPort; 
	
	@Autowired
	protected ActiveExamsPort activeExamsPort;
	
	@Autowired
	protected AddExamsPort addExamsPort;
	
	@Autowired
	protected DeactivateExamPort deactivateExamPort;
	
	@Autowired
	protected UpdateExamPort updateExamPort;
	
	@GetMapping
	public ResponseEntity<Collection<Exam>> getActive() {
		Collection<Exam> exams = this.activeExamsPort.getActiveExams();
		return ResponseEntity.ok(exams);
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Exam> create(@Valid @RequestBody ExamDTO dto) {
		Exam created = this.addExamsPort
				.addExam(dto.toAddCommand());
		return new ResponseEntity<>(created, HttpStatus.CREATED);
	}
	
	@PatchMapping(path = "{examID}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Exam> update(
			@PathVariable("examID") String id,
			@RequestBody ExamDTO dto
	) {
		dto.id = id;
		Exam updated = this.updateExamPort
				.updateExam(dto.toUpdateCommand());

		return new ResponseEntity<>(updated, HttpStatus.OK);
	}
	
	@DeleteMapping(path = "{examID}")
	public ResponseEntity<?> deactivate(@PathVariable("examID") String id) {
		this.deactivateExamPort
			.deactivateExam(UUID.fromString(id));
		return ResponseEntity.ok().build();
	}
	
	@PostMapping(path = "batch", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<Exam>> createMany(@Valid @RequestBody BatchDTO<ExamDTO> dtos) {
		List<AddExamCommand> commands = dtos.batch.stream()
			.map(dto -> dto.toAddCommand())
			.collect(Collectors.toList());
		Collection<Exam> result = this.addExamsPort.addExam(commands);
		return new ResponseEntity<>(result, HttpStatus.CREATED);
	}
	
	@PatchMapping(path = "batch", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<Exam>> batchUpdate(@RequestBody BatchDTO<ExamDTO> dtos) {
		List<UpdateExamCommand> commands = dtos.batch.stream()
			.map(dto -> dto.toUpdateCommand())
			.collect(Collectors.toList());
		
		Collection<Exam> result = this.updateExamPort
				.updateExam(commands);

		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@DeleteMapping(path = "batch")
	public ResponseEntity<?> batchDeactivate(@RequestBody BatchDTO<UUID> ids) {
		this.deactivateExamPort
			.deactivateExam(ids.batch);

		return ResponseEntity.ok().build();
	}
	
	@GetMapping(path = "laboratories")
	public ResponseEntity<?> findLaboratoriesByExamName(@RequestParam("name") String name) {
		Collection<Laboratory> laboratories = this.getLaboratoriesPort.getLaboratoriesByExamName(name);
		return ResponseEntity.ok(laboratories);
	}

}
