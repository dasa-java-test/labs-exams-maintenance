package br.com.fas.dasa.labsexamsmaintenance.services.exams;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Exam;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.out.exams.ExamsRepository;
import br.com.fas.dasa.labsexamsmaintenance.domain.services.exams.DeactivateExamService;
import br.com.fas.dasa.labsexamsmaintenance.mocks.ExamsMocks;

@RunWith(MockitoJUnitRunner.class)
public class DeactivateExamServiceTests {
	
	@Mock
	protected ExamsRepository examsRepository;
	
	@InjectMocks
	protected DeactivateExamService deactivateExamService;
	
	@Test
	public void Should_Deactivate_A_Exam() {
		Exam mock = ExamsMocks.get();
		Exam exam = mock;
		exam.deactivate();
		
		Mockito.when(examsRepository.findExamByID(mock.getId()))
			.thenReturn(Optional.of(mock));
		
		Mockito.when(examsRepository.addOrUpdateExam(exam))
			.thenReturn(exam);
		
		assertDoesNotThrow(() -> deactivateExamService
				.deactivateExam(mock.getId()));
	}
	
}
