package br.com.fas.dasa.labsexamsmaintenance.services.laboratories;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Laboratory;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.out.laboratories.LaboratoryRepository;
import br.com.fas.dasa.labsexamsmaintenance.domain.services.laboratories.GetLaboratoriesService;
import br.com.fas.dasa.labsexamsmaintenance.mocks.LaboratoryMocks;

@RunWith(MockitoJUnitRunner.class)
public class ActiveLaboratoriesServiceTests {
	
	@Mock
	protected LaboratoryRepository laboratoryRepository;
	
	@InjectMocks
	protected GetLaboratoriesService getLaboratoriesService;
	
	@Test
	public void Should_Get_Active_Laboratories() {
		Laboratory laboratory = LaboratoryMocks.get();
		Laboratory otherLab = LaboratoryMocks.get();
		otherLab.deactivate();

		Mockito.when(laboratoryRepository.findAllActiveLaboratories())
			.thenReturn(List.of(laboratory, otherLab));
		
		Collection<Laboratory> activeLaboratories = getLaboratoriesService
				.getActiveLaboratories();
		
		assertNotNull(activeLaboratories);
		assertEquals(1, activeLaboratories.size());
	}

}
