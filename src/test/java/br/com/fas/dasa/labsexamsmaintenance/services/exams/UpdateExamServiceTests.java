package br.com.fas.dasa.labsexamsmaintenance.services.exams;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Exam;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.exams.UpdateExamPort.UpdateExamCommand;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.out.exams.ExamsRepository;
import br.com.fas.dasa.labsexamsmaintenance.domain.services.exams.UpdateExamService;
import br.com.fas.dasa.labsexamsmaintenance.mocks.ExamsMocks;

@RunWith(MockitoJUnitRunner.class)
public class UpdateExamServiceTests {
	
	@Mock
	protected ExamsRepository examsRepository;
	
	@InjectMocks
	protected UpdateExamService updateExamService;
	
	@Test
	public void Should_Update_A_Exam() {
		UUID uuid = UUID.randomUUID();
		
		Exam exam = ExamsMocks.get();
		exam.setId(uuid);
		
		UpdateExamCommand updateCommand = new UpdateExamCommand(
				uuid, "A new name", null, null);
		
		Exam expected = exam;
		expected.setName("A new name");
		
		Mockito.when(examsRepository.findExamByID(uuid))
			.thenReturn(Optional.of(exam));
		
		Mockito.when(examsRepository.addOrUpdateExam(expected))
			.thenReturn(expected);
		
		Exam result = updateExamService.updateExam(updateCommand);
		
		assertNotNull(result);
		assertEquals(expected, result);
	}
	
	@Test
	public void Should_Throw_IllegalArgumentException_On_Update_Unknown_Exame() {
		UUID uuid = UUID.randomUUID();
		
		UpdateExamCommand updateCommand = new UpdateExamCommand(
				uuid, "A new name", null, null);
		
		Mockito.when(examsRepository.findExamByID(uuid))
			.thenReturn(Optional.empty());
		
		assertThrows(IllegalArgumentException.class, () -> updateExamService
				.updateExam(updateCommand));
	}

}
