package br.com.fas.dasa.labsexamsmaintenance.services.laboratories;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Address;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.Laboratory;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.laboratories.UpdateLaboratoryPort.UpdateLaboratoryCommand;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.out.laboratories.LaboratoryRepository;
import br.com.fas.dasa.labsexamsmaintenance.domain.services.laboratories.UpdateLaboratoryService;
import br.com.fas.dasa.labsexamsmaintenance.mocks.LaboratoryMocks;

@RunWith(MockitoJUnitRunner.class)
public class UpdateLaboratoryServiceTests {
	
	@Mock
	protected LaboratoryRepository laboratoryRepository;
	
	@InjectMocks
	protected UpdateLaboratoryService updateLaboratoryService;
	
	@Test
	public void Should_Update_A_Laboratory() {
		UUID uuid = UUID.randomUUID();
		
		Laboratory laboratory = LaboratoryMocks.get();
		laboratory.setId(uuid);
		
		Address address = new Address.Builder().withCity("SP").build();
		UpdateLaboratoryCommand updateCommand = new UpdateLaboratoryCommand(
				uuid, "A new name", address, null);
		
		Laboratory expected = laboratory;
		expected.setName("A new name");
		expected.getAddress().setCity("SP");
		
		Mockito.when(laboratoryRepository.findLaboratoryByID(uuid))
			.thenReturn(Optional.of(laboratory));
		
		Mockito.when(laboratoryRepository.addOrUpdateLaboratory(expected))
			.thenReturn(expected);
		
		Laboratory result = updateLaboratoryService.updateLaboratory(updateCommand);
		
		assertNotNull(result);
		assertEquals(expected, result);
	}
	
	@Test
	public void Should_Throw_IllegalArgumentException() {
		UUID uuid = UUID.randomUUID();
		
		UpdateLaboratoryCommand updateCommand = new UpdateLaboratoryCommand(
				uuid, "A new name", null, null);
		
		Mockito.when(laboratoryRepository.findLaboratoryByID(uuid))
			.thenReturn(Optional.empty());
		
		assertThrows(IllegalArgumentException.class, () -> updateLaboratoryService
				.updateLaboratory(updateCommand));
	}

}
