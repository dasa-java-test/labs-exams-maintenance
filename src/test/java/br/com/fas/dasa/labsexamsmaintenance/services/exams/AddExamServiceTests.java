package br.com.fas.dasa.labsexamsmaintenance.services.exams;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Exam;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.Status;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.exams.AddExamsPort.AddExamCommand;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.out.exams.ExamsRepository;
import br.com.fas.dasa.labsexamsmaintenance.domain.services.exams.AddExamsService;
import br.com.fas.dasa.labsexamsmaintenance.mocks.ExamsMocks;

@PrepareForTest({ UUID.class, Exam.class })
@RunWith(PowerMockRunner.class)
public class AddExamServiceTests {

	@Mock
	ExamsRepository examsRepository;

	@InjectMocks
	AddExamsService addExamsService;

	@Test
	public void Should_Add_Laboratory() {
		UUID uuid = UUID.randomUUID();

		Exam exam = ExamsMocks.get();
		exam.setId(uuid);

		PowerMockito.mockStatic(UUID.class);
		PowerMockito.mockStatic(Exam.class);

		PowerMockito.when(UUID.randomUUID()).thenReturn(uuid);
		PowerMockito.when(examsRepository.addOrUpdateExam(exam)).thenReturn(exam);

		 Exam created = addExamsService
				.addExam(new AddExamCommand(exam.getName(), exam.getType()));

		assertNotNull(created);
		assertNotNull(created.getId());
		assertEquals(Status.ACTIVE, created.getStatus());
	}

}
