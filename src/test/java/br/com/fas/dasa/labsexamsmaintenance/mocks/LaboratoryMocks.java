package br.com.fas.dasa.labsexamsmaintenance.mocks;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Address;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.Laboratory;

public class LaboratoryMocks {
	
	private LaboratoryMocks() {}
	
	public static Laboratory get() {
		String name = "Lavoisier";
		
		Address address = new Address.Builder()
			.withCity("São Paulo")
			.withStreet("Rua Domingos Agostim")
			.withNumber("91")
			.withZip("03306-010")
			.build();
		
		return Laboratory.of(name, address);
	}

}
