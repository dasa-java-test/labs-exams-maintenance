package br.com.fas.dasa.labsexamsmaintenance.integration;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.fas.dasa.labsexamsmaintenance.adapters.persistence.exam.JpaExamRepository;
import br.com.fas.dasa.labsexamsmaintenance.adapters.web.rest.dtos.BatchDTO;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.Exam;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.Laboratory;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.Status;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.out.exams.ExamsRepository;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.out.laboratories.LaboratoryRepository;
import br.com.fas.dasa.labsexamsmaintenance.domain.services.laboratories.AssociateLaboratoryToExamService;
import br.com.fas.dasa.labsexamsmaintenance.mocks.ExamsMocks;
import br.com.fas.dasa.labsexamsmaintenance.mocks.LaboratoryMocks;

public class ExamIntegrationTests extends ApplicationIntegrationTests {
	
	@Autowired
	protected ExamsRepository examsRepository;
	
	@Autowired
	protected JpaExamRepository jpaExamsRepository;
	
	@Autowired
	protected LaboratoryRepository labRepo;
	
	@Autowired
	protected AssociateLaboratoryToExamService associateLaboratoryToExamService;

	protected static final String BASE_URL = "/exams";
	
	private static final ObjectMapper mapper = new ObjectMapper();
	
	@Before
	public void dropBase() {
		jpaExamsRepository.deleteAll();
	}
	
	@Test
	public void Should_Get_Exams() throws Exception {
		Exam exam = ExamsMocks.get();
		Exam deactivated = ExamsMocks.get();
		deactivated.deactivate();
		
		examsRepository.bulkAddOrUpdateExam(List.of(exam, deactivated));
		
		mockMvc.perform(get(BASE_URL)).andExpect(status().isOk())
	    	.andExpect(jsonPath("$").isArray())
	    	.andExpect(jsonPath("$", hasSize(1)));
	}
	
	@Test
	public void Should_Create_Exam() throws Exception {
		Exam exam = ExamsMocks.get();
		String json = mapper.writeValueAsString(exam);
		mockMvc.perform(
				post(BASE_URL)
					.contentType(MediaType.APPLICATION_JSON).content(json)
				)
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.name").value(exam.getName()));
	}
	
	@Test
	public void Should_Not_Create_Exam() throws Exception {
		Exam exam = ExamsMocks.get();
		exam.setName(null);
		String json = mapper.writeValueAsString(exam);
		mockMvc.perform(
				post(BASE_URL)
					.contentType(MediaType.APPLICATION_JSON).content(json)
				)
				.andExpect(status().is(400));
	}
	
	@Test
	public void Should_Update_Exam_Name() throws Exception {
		Exam exam = ExamsMocks.get();
		examsRepository.addOrUpdateExam(exam);
		String json = "{\"name\":\"Mudando\"}";
		String uri = String.format("%s/%s", BASE_URL, exam.getId());
		mockMvc.perform(
				patch(uri)
					.contentType(MediaType.APPLICATION_JSON).content(json)
				)
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.name").value("Mudando"));
	}
	
	@Test
	public void Should_Disable_Exam() throws Exception {
		Exam exam = ExamsMocks.get();
		examsRepository.addOrUpdateExam(exam);
		String uri = String.format("%s/%s", BASE_URL, exam.getId());
		
		mockMvc.perform(delete(uri))
				.andExpect(status().isOk());
		 
		 assertExamIsDisabled(exam);
	}
	
	@Test
	public void Should_Batch_Add_Exams() throws Exception {
		Exam examA = ExamsMocks.get();
		Exam examB = ExamsMocks.get();
		BatchDTO<Exam> batch = new BatchDTO<>(examA, examB);
		String json = mapper.writeValueAsString(batch);
		String uri = String.format("%s/batch", BASE_URL);
		mockMvc.perform(
				post(uri)
					.contentType(MediaType.APPLICATION_JSON).content(json)
				)
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$").isArray())
		    	.andExpect(jsonPath("$", hasSize(2)));	
	}
	
	@Test
	public void Should_Batch_Update_Exams() throws Exception {
		Exam examA = ExamsMocks.get();
		Exam examB = ExamsMocks.get();
		
		examsRepository.bulkAddOrUpdateExam(List.of(examA, examB));
		
		examA.setName("Exam A");
		examB.setName("Exam B");
		
		BatchDTO<Exam> batch = new BatchDTO<>(examA, examB);
		String json = mapper.writeValueAsString(batch);
		
		String uri = String.format("%s/batch", BASE_URL);
		
		mockMvc.perform(
				patch(uri)
					.contentType(MediaType.APPLICATION_JSON).content(json)
				)
				.andExpect(status().isOk())
				.andExpect(jsonPath("$").isArray())
		    	.andExpect(jsonPath("$", hasSize(2)));	
	}
	
	@Test
	public void Should_Batch_Disable_Exams() throws Exception {
		Exam examA = ExamsMocks.get();
		Exam examB = ExamsMocks.get();
		
		examsRepository.bulkAddOrUpdateExam(List.of(examA, examB));
		
		BatchDTO<UUID> batch = new BatchDTO<>(
				examA.getId(), examB.getId());

		String json = mapper.writeValueAsString(batch);
		
		String uri = String.format("%s/batch", BASE_URL);
		
		mockMvc.perform(
				delete(uri)
					.contentType(MediaType.APPLICATION_JSON).content(json)
				)
				.andExpect(status().isOk());
		
		assertExamIsDisabled(examA);
		assertExamIsDisabled(examB);
	}
	
	@Test
	public void Should_Fetch_Laboratories_By_Exam_Name() throws Exception {
		Laboratory laboratory = LaboratoryMocks.get();
		Exam exam = ExamsMocks.get();
		Exam examB = ExamsMocks.get();
		examB.setName("Other exam");
		
		labRepo.addOrUpdateLaboratory(laboratory);
		examsRepository.bulkAddOrUpdateExam(List.of(exam, examB));
		
		associateLaboratoryToExamService
			.associateExamToLaboratory(laboratory.getId(), exam.getId());
		
		String uri = String.format("%s/laboratories?name=%s", BASE_URL, exam.getName());

		mockMvc.perform(get(uri))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$").isArray())
		    	.andExpect(jsonPath("$", hasSize(1)));
	}
	
	private void assertExamIsDisabled(Exam exam) {
		Optional<Exam> optional = examsRepository.findExamByID(exam.getId());
		assertEquals(true, optional.isPresent());
		assertEquals(Status.INACTIVE, optional.get().getStatus());
	}
	
}
