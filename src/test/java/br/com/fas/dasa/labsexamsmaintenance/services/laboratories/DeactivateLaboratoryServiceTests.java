package br.com.fas.dasa.labsexamsmaintenance.services.laboratories;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Laboratory;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.out.laboratories.LaboratoryRepository;
import br.com.fas.dasa.labsexamsmaintenance.domain.services.laboratories.DeactivateLaboratoryService;
import br.com.fas.dasa.labsexamsmaintenance.mocks.LaboratoryMocks;

@RunWith(MockitoJUnitRunner.class)
public class DeactivateLaboratoryServiceTests {
	
	@Mock
	protected LaboratoryRepository laboratoryRepository;
	
	@InjectMocks
	protected DeactivateLaboratoryService deactivateLaboratoryService;
	
	@Test
	public void Should_Deactivate_A_Laboratory() {
		Laboratory mockLab = LaboratoryMocks.get();
		Laboratory laboratory = mockLab;
		laboratory.deactivate();
		
		Mockito.when(laboratoryRepository.findLaboratoryByID(mockLab.getId()))
			.thenReturn(Optional.of(mockLab));
		
		Mockito.when(laboratoryRepository.addOrUpdateLaboratory(laboratory))
			.thenReturn(laboratory);
		
		assertDoesNotThrow(() -> deactivateLaboratoryService
				.deactivateLaboratory(mockLab.getId()));
	}
	
}
