package br.com.fas.dasa.labsexamsmaintenance.services.laboratories;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Exam;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.Laboratory;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.out.exams.ExamsRepository;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.out.laboratories.LaboratoryRepository;
import br.com.fas.dasa.labsexamsmaintenance.domain.services.laboratories.AssociateLaboratoryToExamService;
import br.com.fas.dasa.labsexamsmaintenance.mocks.ExamsMocks;
import br.com.fas.dasa.labsexamsmaintenance.mocks.LaboratoryMocks;

@RunWith(MockitoJUnitRunner.class)
public class AssociateLaboratoryToExamTests {
	
	@Mock
	protected LaboratoryRepository laboratoryRepository;
	
	@Mock
	protected ExamsRepository examsRepository;
	
	@InjectMocks
	protected AssociateLaboratoryToExamService associateLaboratoryToExamService;
	
	@Test
	public void Should_Associate_Lab_To_Exam() {
		Laboratory lab = LaboratoryMocks.get();
		Exam exam = ExamsMocks.get();
		
		Laboratory result = lab;
		result.associateExam(exam);
		
		Mockito.when(laboratoryRepository.findActiveLaboratoryByID(lab.getId()))
			.thenReturn(Optional.of(lab));
		
		Mockito.when(examsRepository.findExamByID(exam.getId()))
			.thenReturn(Optional.of(exam));
		
		Mockito.when(laboratoryRepository.addOrUpdateLaboratory(result))
			.thenReturn(result);
		
		assertDoesNotThrow(() -> associateLaboratoryToExamService
				.associateExamToLaboratory(lab.getId(), exam.getId()));
	}
	
	@Test
	public void Should_Not_Associate_Missing_Exam() {
		UUID examID = UUID.randomUUID();
		Laboratory lab = LaboratoryMocks.get();
		
		Mockito.when(laboratoryRepository.findActiveLaboratoryByID(lab.getId()))
			.thenReturn(Optional.of(lab));
		
		Mockito.when(examsRepository.findExamByID(examID))
			.thenReturn(Optional.empty());
		
		assertThrows(IllegalArgumentException.class, () -> associateLaboratoryToExamService
				.associateExamToLaboratory(lab.getId(), examID));
	}
	
	@Test
	public void Should_Not_Associate_Inactive_Exam() {
		Laboratory lab = LaboratoryMocks.get();
		Exam exam = ExamsMocks.get();
		exam.deactivate();
		
		Mockito.when(laboratoryRepository.findActiveLaboratoryByID(lab.getId()))
			.thenReturn(Optional.of(lab));
		
		Mockito.when(examsRepository.findExamByID(exam.getId()))
			.thenReturn(Optional.of(exam));
		
		assertThrows(IllegalArgumentException.class, () -> associateLaboratoryToExamService
				.associateExamToLaboratory(lab.getId(), exam.getId()));
	}
	
	@Test
	public void Should_Not_Associate_Inactive_Laboratory() {
		Laboratory lab = LaboratoryMocks.get();
		lab.deactivate();
		
		Mockito.when(laboratoryRepository.findActiveLaboratoryByID(lab.getId()))
			.thenReturn(Optional.empty());
		
		assertThrows(IllegalArgumentException.class, () -> associateLaboratoryToExamService
				.associateExamToLaboratory(lab.getId(), UUID.randomUUID()));
	}

}
