package br.com.fas.dasa.labsexamsmaintenance.services.exams;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Exam;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.out.exams.ExamsRepository;
import br.com.fas.dasa.labsexamsmaintenance.domain.services.exams.ActiveExamsService;
import br.com.fas.dasa.labsexamsmaintenance.mocks.ExamsMocks;

@RunWith(MockitoJUnitRunner.class)
public class ActiveExamsServiceTests {
	
	@Mock
	protected ExamsRepository examsRepository;
	
	@InjectMocks
	protected ActiveExamsService activeExamsService;
	
	@Test
	public void Should_Get_Active_Exams() {
		Exam exam = ExamsMocks.get();
		Exam otherExam = ExamsMocks.get();
		otherExam.deactivate();

		Mockito.when(examsRepository.findAllActiveExams())
			.thenReturn(List.of(exam, otherExam));
		
		Collection<Exam> activeExams = activeExamsService
				.getActiveExams();
		
		assertNotNull(activeExams);
		assertEquals(1, activeExams.size());
	}

}
