package br.com.fas.dasa.labsexamsmaintenance.mocks;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Exam;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.ExamType;

public class ExamsMocks {

	private ExamsMocks() {
	}

	public static Exam get() {
		return new Exam("A exam", ExamType.CLINICAL_ANALYSIS);
	}

}
