package br.com.fas.dasa.labsexamsmaintenance.integration;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.fas.dasa.labsexamsmaintenance.adapters.persistence.laboratory.JpaLaboratoryRepository;
import br.com.fas.dasa.labsexamsmaintenance.adapters.web.rest.dtos.BatchDTO;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.Exam;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.Laboratory;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.Status;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.out.exams.ExamsRepository;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.out.laboratories.LaboratoryRepository;
import br.com.fas.dasa.labsexamsmaintenance.mocks.ExamsMocks;
import br.com.fas.dasa.labsexamsmaintenance.mocks.LaboratoryMocks;

public class LaboratoryIntegrationTests extends ApplicationIntegrationTests {
	
	@Autowired
	protected LaboratoryRepository labRepository;
	
	@Autowired
	protected JpaLaboratoryRepository jpaLabRepository;
	
	@Autowired
	protected ExamsRepository examRepository;
	
	protected static final String BASE_URL = "/laboratories";
	
	private static final ObjectMapper mapper = new ObjectMapper();
	
	@Before
	public void dropBase() {
		jpaLabRepository.deleteAll();
	}
	
	@Test
	public void Should_Get_Laboratories() throws Exception {
		Laboratory laboratory = LaboratoryMocks.get();
		Laboratory deactivated = LaboratoryMocks.get();
		deactivated.deactivate();
		
		labRepository.bulkAddOrUpdate(List.of(laboratory, deactivated));
		
		mockMvc.perform(get(BASE_URL)).andExpect(status().isOk())
	    	.andExpect(jsonPath("$").isArray())
	    	.andExpect(jsonPath("$", hasSize(1)));
	}
	
	@Test
	public void Should_Create_Laboratory() throws Exception {
		Laboratory laboratory = LaboratoryMocks.get();
		String json = mapper.writeValueAsString(laboratory);
		mockMvc.perform(
				post(BASE_URL)
					.contentType(MediaType.APPLICATION_JSON).content(json)
				)
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.name").value(laboratory.getName()));
	}
	
	@Test
	public void Should_Not_Create_Laboratory() throws Exception {
		Laboratory laboratory = LaboratoryMocks.get();
		laboratory.setName(null);
		String json = mapper.writeValueAsString(laboratory);
		mockMvc.perform(
				post(BASE_URL)
					.contentType(MediaType.APPLICATION_JSON).content(json)
				)
				.andExpect(status().is(400));
	}
	
	@Test
	public void Should_Update_Laboratory_Name() throws Exception {
		Laboratory laboratory = LaboratoryMocks.get();
		labRepository.addOrUpdateLaboratory(laboratory);
		String json = "{\"name\":\"Mudando\"}";
		String uri = String.format("%s/%s", BASE_URL, laboratory.getId());
		mockMvc.perform(
				patch(uri)
					.contentType(MediaType.APPLICATION_JSON).content(json)
				)
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.name").value("Mudando"));
	}
	
	@Test
	public void Should_Update_Laboratory_Address() throws Exception {
		Laboratory laboratory = LaboratoryMocks.get();
		labRepository.addOrUpdateLaboratory(laboratory);
		String json = "{\"address\":{\"city\":\"BH\"}}";
		String uri = String.format("%s/%s", BASE_URL, laboratory.getId());
		mockMvc.perform(
				patch(uri)
					.contentType(MediaType.APPLICATION_JSON).content(json)
				)
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.address.city").value("BH"));
	}
	
	@Test
	public void Should_Disable_Laboratory() throws Exception {
		Laboratory laboratory = LaboratoryMocks.get();
		labRepository.addOrUpdateLaboratory(laboratory);
		String uri = String.format("%s/%s", BASE_URL, laboratory.getId());
		
		mockMvc.perform(delete(uri))
				.andExpect(status().isOk());
		
		assertLabIsDisabled(laboratory);
	}
	
	@Test
	public void Should_Bind_Exam_To_Laboratory() throws Exception {
		Laboratory laboratory = LaboratoryMocks.get();
		Exam exam = ExamsMocks.get();
		
		examRepository.addOrUpdateExam(exam);
		labRepository.addOrUpdateLaboratory(laboratory);
		
		String uri = String.format("%s/%s/bind/%s", BASE_URL, laboratory.getId(), exam.getId());
		
		mockMvc.perform(patch(uri))
			.andExpect(status().isOk());
	}
	
	@Test
	public void Should_Batch_Add_Laboratories() throws Exception {
		Laboratory labA = LaboratoryMocks.get();
		Laboratory labB = LaboratoryMocks.get();
		BatchDTO<Laboratory> batch = new BatchDTO<>(labA, labB);
		String json = mapper.writeValueAsString(batch);
		String uri = String.format("%s/batch", BASE_URL);
		mockMvc.perform(
				post(uri)
					.contentType(MediaType.APPLICATION_JSON).content(json)
				)
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$").isArray())
		    	.andExpect(jsonPath("$", hasSize(2)));	
	}
	
	@Test
	public void Should_Batch_Update_Laboratories() throws Exception {
		Laboratory labA = LaboratoryMocks.get();
		Laboratory labB = LaboratoryMocks.get();
		
		labRepository.bulkAddOrUpdate(List.of(labA, labB));
		
		labA.setName("Lab A");
		labB.setName("Lab B");
		
		BatchDTO<Laboratory> batch = new BatchDTO<>(labA, labB);
		String json = mapper.writeValueAsString(batch);
		
		String uri = String.format("%s/batch", BASE_URL);
		
		mockMvc.perform(
				patch(uri)
					.contentType(MediaType.APPLICATION_JSON).content(json)
				)
				.andExpect(status().isOk())
				.andExpect(jsonPath("$").isArray())
		    	.andExpect(jsonPath("$", hasSize(2)));	
	}
	
	@Test
	public void Should_Batch_Disable_Laboratories() throws Exception {
		Laboratory labA = LaboratoryMocks.get();
		Laboratory labB = LaboratoryMocks.get();
		
		labRepository.bulkAddOrUpdate(List.of(labA, labB));
		
		BatchDTO<UUID> batch = new BatchDTO<>(
				labA.getId(), labB.getId());

		String json = mapper.writeValueAsString(batch);
		
		String uri = String.format("%s/batch", BASE_URL);
		
		mockMvc.perform(
				delete(uri)
					.contentType(MediaType.APPLICATION_JSON).content(json)
				)
				.andExpect(status().isOk());
		
		assertLabIsDisabled(labA);
		assertLabIsDisabled(labB);
	}
	
	private void assertLabIsDisabled(Laboratory lab) {
		Optional<Laboratory> optional = labRepository.findLaboratoryByID(lab.getId());
		assertEquals(true, optional.isPresent());
		assertEquals(Status.INACTIVE, optional.get().getStatus());
	}

}
