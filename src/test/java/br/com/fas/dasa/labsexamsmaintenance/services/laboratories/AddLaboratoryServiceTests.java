package br.com.fas.dasa.labsexamsmaintenance.services.laboratories;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import br.com.fas.dasa.labsexamsmaintenance.domain.models.Laboratory;
import br.com.fas.dasa.labsexamsmaintenance.domain.models.Status;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.in.laboratories.AddLaboratoryPort.AddLaboratoryCommand;
import br.com.fas.dasa.labsexamsmaintenance.domain.ports.out.laboratories.LaboratoryRepository;
import br.com.fas.dasa.labsexamsmaintenance.domain.services.laboratories.AddLaboratoryService;
import br.com.fas.dasa.labsexamsmaintenance.mocks.LaboratoryMocks;

@PrepareForTest({ UUID.class, Laboratory.class })
@RunWith(PowerMockRunner.class)
public class AddLaboratoryServiceTests {

	@Mock
	LaboratoryRepository addLaboratoryRepository;

	@InjectMocks
	AddLaboratoryService addLaboratoryService;

	@Test
	public void Should_Add_Laboratory() {
		UUID uuid = UUID.randomUUID();

		Laboratory lab = LaboratoryMocks.get();
		lab.setId(uuid);

		PowerMockito.mockStatic(UUID.class);
		PowerMockito.mockStatic(Laboratory.class);

		PowerMockito.when(UUID.randomUUID()).thenReturn(uuid);
		PowerMockito.when(Laboratory.of(lab.getName(), lab.getAddress())).thenReturn(lab);
		PowerMockito.when(addLaboratoryRepository.addOrUpdateLaboratory(lab)).thenReturn(lab);

		Laboratory created = addLaboratoryService
				.addLaboratory(new AddLaboratoryCommand(lab.getName(), lab.getAddress()));

		assertNotNull(created);
		assertNotNull(created.getId());
		assertEquals(Status.ACTIVE, created.getStatus());
	}

}
